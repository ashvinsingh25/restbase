package io.gitlab.restbase.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoutingConfiguration {

  @Autowired
  RestbaseProperties config;
  
  @Bean
  public RouteLocator routes(RouteLocatorBuilder builder) {
    return builder.routes()
      .route(p -> p.path("/api")
        .filters(f -> f.addRequestHeader("through", "restbase"))
        .uri(config.getApi()))
      .route("websocket_route", r -> r.path("/echo")
          .uri("ws://localhost:9000"))
      .build();
  }
}

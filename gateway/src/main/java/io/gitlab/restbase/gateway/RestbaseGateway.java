package io.gitlab.restbase.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RestbaseProperties.class)
public class RestbaseGateway {

	public static void main(String[] args) {
		SpringApplication.run(RestbaseGateway.class, args);
	}

}


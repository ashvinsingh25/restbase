package io.gitlab.restbase.gateway;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "restbase")
public class RestbaseProperties {

  private String api;

  public String getApi() {
    return api;
  }

  public void setApi(String api) {
    this.api = api;
  }

}

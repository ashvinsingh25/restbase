package io.gitlab.restbase.server.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest

public class DatabaseMigrationPropertiesTest {

  @Autowired
  DatabaseMigrationProperties properties;

  @Test
  public void notNull() {
    assertNotNull(properties);
  }

  @Test
  public void initScript() {
    assertTrue(properties.hasInitScript());
    assertEquals("db/migration/V0__init.sql", properties.getInitScript());
  }

  @Test
  public void cleanScript() {
    assertFalse(properties.hasCleanScript());
    assertEquals("", properties.getCleanScript());
  }
}

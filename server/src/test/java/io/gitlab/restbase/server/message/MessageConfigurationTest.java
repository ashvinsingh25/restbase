package io.gitlab.restbase.server.message;

import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = NONE)

public class MessageConfigurationTest {

  @Autowired
  RabbitProperties rabbit;

  @Autowired
  MessageConfiguration messageConfig;

  @Autowired
  ConnectionFactory connectionFactory;

  @Test
  public void testConnectionFactory() {
    assertNotNull(connectionFactory);
  }

  @Test
  public void testConfiguration() {
    assertNotNull(messageConfig);
    assertNotNull(messageConfig.rabbitConnectionFactory(rabbit));
  }

}

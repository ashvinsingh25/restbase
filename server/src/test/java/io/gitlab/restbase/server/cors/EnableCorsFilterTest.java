package io.gitlab.restbase.server.cors;

import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_ALLOW_HEADERS;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_ALLOW_HEADERS_VALUE;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_ALLOW_METHODS;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_ALLOW_METHODS_VALUE;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_ALLOW_ORIGIN;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_ALLOW_ORIGIN_VALUE;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_MAX_AGE;
import static io.gitlab.restbase.server.cors.EnableCorsFilter.ACCESS_CONTROL_MAX_AGE_VALUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import io.gitlab.restbase.DocumentWithId;

@RunWith(SpringRunner.class)
@SpringBootTest

public class EnableCorsFilterTest {

  @Autowired
  private ApplicationContext context;

  WebTestClient rest;

  @Before
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
      .build();
  }

  @Test
  public void testFilter() {
    EntityExchangeResult<DocumentWithId> result = rest.get()
      .uri("/api")
      .exchange()
      .expectStatus()
      .isOk()
      .expectBody(DocumentWithId.class)
      .returnResult();

    assertNotNull(result);
    HttpHeaders responseHeaders = result.getResponseHeaders();

    assertNotNull(responseHeaders);
    assertEquals(responseHeaders.getFirst(ACCESS_CONTROL_ALLOW_ORIGIN), ACCESS_CONTROL_ALLOW_ORIGIN_VALUE);
    assertEquals(responseHeaders.getFirst(ACCESS_CONTROL_ALLOW_METHODS), ACCESS_CONTROL_ALLOW_METHODS_VALUE);
    assertEquals(responseHeaders.getFirst(ACCESS_CONTROL_ALLOW_HEADERS), ACCESS_CONTROL_ALLOW_HEADERS_VALUE);
    assertEquals(responseHeaders.getFirst(ACCESS_CONTROL_MAX_AGE), ACCESS_CONTROL_MAX_AGE_VALUE);
  }

}

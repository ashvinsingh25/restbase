package io.gitlab.restbase.server.message;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableRabbit
@Configuration
public class TestConfiguration {
  private static final int CONCURRENT_CONSUMERS_MAX = 10;
  private static final int CONCURRENT_CONSUMERS_DEFAULT = 3;

  @Bean
  public SimpleRabbitListenerContainerFactory createListenerFactory(ConnectionFactory connectionFactory) {
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    factory.setConnectionFactory(connectionFactory);
    factory.setConcurrentConsumers(CONCURRENT_CONSUMERS_DEFAULT);
    factory.setMaxConcurrentConsumers(CONCURRENT_CONSUMERS_MAX);
    factory.setAcknowledgeMode(AcknowledgeMode.NONE);
    return factory;
  }

}

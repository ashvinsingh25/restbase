package io.gitlab.restbase.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;

@RunWith(SpringRunner.class)
@SpringBootTest

public class CollectionControllerTest {

  @Autowired
  private ApplicationContext context;

  @Autowired
  private CollectionRepository collections;

  @Autowired
  private DocumentRepository documents;

  WebTestClient rest;

  @Before
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
      .configureClient()
      .responseTimeout(Duration.ofHours(1L))
      .build();
  }

  @Before
  public void cleanUp() {
    collections.deleteAll()
      .block();
    documents.deleteById(documents.findAll()
      .map(doc -> doc.getId())
      .filter(id -> id != Document.ROOT_ID))
      .block();
  }

  @Test
  public void createAndGetCollectionAtLevel1() {
    Collection collection1 = new Collection();
    collection1.setName("test");

    Collection inserted = rest.post()
      .uri("/api")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("test", inserted.getName());

    rest.get()
      .uri("/api/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetCollectionAtLevel2() {
    Collection collection1 = new Collection();
    collection1.setName("test");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("test", inserted.getName());

    rest.get()
      .uri("/api/col1/doc1/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetCollectionAtLevel3() {
    Collection collection1 = new Collection();
    collection1.setName("test");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("test", inserted.getName());

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetCollectionAtLevel4() {
    Collection collection1 = new Collection();
    collection1.setName("test");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("test", inserted.getName());

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetCollectionAtLevel5() {
    Collection collection1 = new Collection();
    collection1.setName("test");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("test", inserted.getName());

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndDeleteCollectionAtLevel1() {
    Collection collection1 = new Collection();
    collection1.setName("test");

    Collection inserted = rest.post()
      .uri("/api")
      .accept(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk();

    rest.get()
      .uri("/api/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteCollectionAtLevel2() {
    Collection collection1 = new Collection();
    collection1.setName("test-name");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1")
      .accept(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk();

    rest.get()
      .uri("/api/col1/doc1/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteCollectionAtLevel3() {
    Collection collection1 = new Collection();
    collection1.setName("test-name");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2")
      .accept(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/col2/doc2/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteCollectionAtLevel4() {
    Collection collection1 = new Collection();
    collection1.setName("test-name");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3")
      .accept(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteCollectionAtLevel5() {
    Collection collection1 = new Collection();
    collection1.setName("test-name");

    Collection inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4")
      .accept(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isOk();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4/{name}", inserted.getName())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndListCollections() {
    Collection collection1 = new Collection();
    collection1.setName("c1");
    Collection collection2 = new Collection();
    collection2.setName("c2");

    Collection inserted1 = rest.post()
      .uri("/api")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();
    Collection inserted2 = rest.post()
      .uri("/api")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection2)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.get()
      .uri("/api/c1")
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .consumeWith(c1 -> {
        assertNotNull(c1);
      });

    rest.get()
      .uri("/api/collections")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<Collection>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<Collection> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(inserted1));
        assertTrue(list.contains(inserted2));
      });
  }

  @Test
  public void createAndListCollectionsAtLevel2() {
    Collection collection1 = new Collection();
    collection1.setName("c1");
    Collection collection2 = new Collection();
    collection2.setName("c2");

    Collection inserted1 = rest.post()
      .uri("/api/col1/doc1")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();
    Collection inserted2 = rest.post()
      .uri("/api/col1/doc1")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection2)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.get()
      .uri("/api/col1/doc1/collections")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<Collection>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<Collection> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(inserted1));
        assertTrue(list.contains(inserted2));
      });
  }

  @Test
  public void createAndListCollectionsAtLevel3() {
    Collection collection1 = new Collection();
    collection1.setName("c1");
    Collection collection2 = new Collection();
    collection2.setName("c2");

    Collection inserted1 = rest.post()
      .uri("/api/col1/doc1/col2/doc2")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();
    Collection inserted2 = rest.post()
      .uri("/api/col1/doc1/col2/doc2")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection2)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/collections")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<Collection>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<Collection> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(inserted1));
        assertTrue(list.contains(inserted2));
      });
  }

  @Test
  public void createAndListCollectionsAtLevel4() {
    Collection collection1 = new Collection();
    collection1.setName("c1");
    Collection collection2 = new Collection();
    collection2.setName("c2");

    Collection inserted1 = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();
    Collection inserted2 = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection2)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/collections")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<Collection>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<Collection> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(inserted1));
        assertTrue(list.contains(inserted2));
      });
  }

  @Test
  public void createAndListCollectionsAtLevel5() {
    Collection collection1 = new Collection();
    collection1.setName("c1");
    Collection collection2 = new Collection();
    collection2.setName("c2");

    Collection inserted1 = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection1)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();
    Collection inserted2 = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4")
      .accept(APPLICATION_JSON)
      .contentType(APPLICATION_JSON)
      .bodyValue(collection2)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(Collection.class)
      .returnResult()
      .getResponseBody();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4/collections")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<Collection>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<Collection> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(inserted1));
        assertTrue(list.contains(inserted2));
      });
  }

  @Test
  public void createAndListDocumentsAtLevel1() {
    Collection collection = new Collection();
    collection.setName("col1");
    collections.save(collection)
      .block();
    DocumentWithId doc1 = new DocumentWithId(null, collection.path());
    DocumentWithId doc2 = new DocumentWithId(null, collection.path());
    doc1.setCollection(collection.getName());
    doc2.setCollection(collection.getName());
    documents.saveAll(Arrays.asList(doc1, doc2))
      .blockLast();

    rest.get()
      .uri("/api/{name}/documents", collection.getName())
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<DocumentWithId>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<DocumentWithId> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(doc1));
        assertTrue(list.contains(doc2));
      });
  }

  @Test
  public void createAndListDocumentsAtLevel2() {
    Collection collection = new Collection();
    collection.setDocument("col1/doc1");
    collection.setName("col2");
    collections.save(collection)
      .block();
    DocumentWithId doc1 = new DocumentWithId(null, collection.path());
    DocumentWithId doc2 = new DocumentWithId(null, collection.path());
    doc1.setCollection("col1/doc1/col2");
    doc2.setCollection("col1/doc1/col2");
    documents.saveAll(Arrays.asList(doc1, doc2))
      .blockLast();

    List<DocumentWithId> list = rest.get()
      .uri("/api/col1/doc1/col2/documents")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<DocumentWithId>>() {
      })
      .returnResult()
      .getResponseBody();

    assertNotNull(list);
    assertEquals(2, list.size());
    assertTrue(list.contains(doc1));
    assertTrue(list.contains(doc2));
  }

  @Test
  public void createAndListDocumentsAtLevel3() {
    Collection collection = new Collection();
    collection.setDocument("col1/doc1/col2/doc2");
    collection.setName("col3");
    collections.save(collection)
      .block();
    DocumentWithId doc1 = new DocumentWithId(null, collection.path());
    DocumentWithId doc2 = new DocumentWithId(null, collection.path());
    doc1.setCollection("col1/doc1/col2/doc2/col3");
    doc2.setCollection("col1/doc1/col2/doc2/col3");
    documents.saveAll(Arrays.asList(doc1, doc2))
      .blockLast();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/documents")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<DocumentWithId>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<DocumentWithId> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(doc1));
        assertTrue(list.contains(doc2));
      });
  }

  @Test
  public void createAndListDocumentsAtLevel4() {
    Collection collection = new Collection();
    collection.setDocument("col1/doc1/col2/doc2/col3/doc3");
    collection.setName("col4");
    collections.save(collection)
      .block();
    DocumentWithId doc1 = new DocumentWithId(null, collection.path());
    DocumentWithId doc2 = new DocumentWithId(null, collection.path());
    doc1.setCollection("col1/doc1/col2/doc2/col3/doc3/col4");
    doc2.setCollection("col1/doc1/col2/doc2/col3/doc3/col4");
    documents.saveAll(Arrays.asList(doc1, doc2))
      .blockLast();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/documents")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<DocumentWithId>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<DocumentWithId> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(doc1));
        assertTrue(list.contains(doc2));
      });
  }

  @Test
  public void createAndListDocumentsAtLevel5() {
    Collection collection = new Collection();
    collection.setDocument("col1/doc1/col2/doc2/col3/doc3/col4/doc4");
    collection.setName("col4");
    collections.save(collection)
      .block();
    DocumentWithId doc1 = new DocumentWithId(null, collection.path());
    DocumentWithId doc2 = new DocumentWithId(null, collection.path());
    doc1.setCollection("col1/doc1/col2/doc2/col3/doc3/col4/doc4/col5");
    doc2.setCollection("col1/doc1/col2/doc2/col3/doc3/col4/doc4/col5");
    documents.saveAll(Arrays.asList(doc1, doc2))
      .blockLast();

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/doc4/col5/documents")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<DocumentWithId>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<DocumentWithId> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains(doc1));
        assertTrue(list.contains(doc2));
      });
  }

  @Test
  @Ignore("case not implemented yet")
  public void createAndListWithoutDocuments() {
    Collection collection = new Collection();
    collection.setName("col1");
    collections.save(collection)
      .block();

    rest.get()
      .uri("/api/{name}/documents", collection.getName())
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(new ParameterizedTypeReference<List<Document>>() {
      })
      .consumeWith(response -> {
        assertNotNull(response);
        List<Document> list = response.getResponseBody();
        assertNotNull(list);
        assertEquals(0, list.size());
      });
  }

  @Test
  public void getMissingCollection() {
    rest.get()
      .uri("/api/{name}", "missing")
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void getDocumentsOfMissingCollection() {
    rest.get()
      .uri("/api/{name}/documents", "missing")
      .exchange()
      .expectStatus()
      .isNotFound();
  }

}

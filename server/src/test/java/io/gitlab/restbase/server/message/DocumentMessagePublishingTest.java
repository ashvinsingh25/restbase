package io.gitlab.restbase.server.message;

import static io.gitlab.restbase.PathUtils.path;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableMap;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.server.DocumentRepository;
import io.gitlab.restbase.server.DocumentService;
import reactor.core.publisher.Mono;

public class DocumentMessagePublishingTest {

  DocumentService service;

  DocumentRepository repository;

  Publisher publisher;

  @Before
  public void setUp() {
    repository = mock(DocumentRepository.class);
    publisher = mock(Publisher.class);
    service = new DocumentService(repository, publisher);
  }

  @Test(timeout = 5000)
  public void updateRootDocument() throws Exception {

    Path path = path("a");
    DocumentWithId root = DocumentWithId.ROOT;

    DocumentWithId updated = new DocumentWithId(42L, path, ImmutableMap.of("f1", "val1", "f2", "val2"));
    when(repository.save(Mockito.any(DocumentWithId.class))).thenReturn(Mono.just(updated));

    service.update(root.path(), root)
      .block();

    verify(publisher, times(1)).publishDocumentUpdated(updated);
  }

  @Test(timeout = 5000)
  public void updateDocumentAtLevel2() throws Exception {
    ImmutableMap<String, Object> fields = ImmutableMap.of("f1", "val1");

    Path path = path("a");
    DocumentWithId saved = new DocumentWithId(42L, path, fields);

    DocumentWithId updated = new DocumentWithId(42L, path, ImmutableMap.of("f1", "val1", "f2", "val2"));
    when(repository.save(saved)).thenReturn(Mono.just(updated));

    service.update(saved.path(), saved)
      .block();

    verify(publisher, times(1)).publishDocumentUpdated(updated);
  }

  public void createDocumentAtLevel2() throws Exception {
    ImmutableMap<String, Object> fields = ImmutableMap.of("f1", "val1");
    Document doc = new Document(fields);

    Path path = path("a");
    DocumentWithId saved = new DocumentWithId(42L, path, fields);

    DocumentWithId unsaved = new DocumentWithId(null, path, fields);
    when(repository.save(unsaved)).thenReturn(Mono.just(saved));

    service.save(path, doc)
      .block();

    verify(publisher, times(1)).publishDocumentAdded(saved);
  }

  @Test(timeout = 5000)
  public void createDocumentAtLevel4() throws Exception {
    ImmutableMap<String, Object> fields = ImmutableMap.of("f1", "val1");
    Document doc = new Document(fields);

    Path path = path("a", "b", "c");
    DocumentWithId saved = new DocumentWithId(42L, path, fields);

    DocumentWithId unsaved = new DocumentWithId(null, path, fields);
    when(repository.save(unsaved)).thenReturn(Mono.just(saved));

    service.save(path, doc)
      .block();

    verify(publisher, times(1)).publishDocumentAdded(saved);
  }

  @Test(timeout = 5000)
  public void deleteDocumentAtLevel2() throws Exception {
    DocumentWithId doc = new DocumentWithId(42L, path("path"));
    doc.set("f1", "val1");

    Long id = doc.getId();
    when(repository.deleteById(id)).thenReturn(Mono.empty());
    when(repository.findById(id)).thenReturn(Mono.just(doc));
    Path path = path("a", "" + id);
    service.delete(path)
      .block();

    verify(publisher, times(1)).publishDocumentDeleted(doc);
  }

  @Test(timeout = 5000)
  public void deleteDocumentAtLevel4() throws Exception {
    DocumentWithId doc = new DocumentWithId(42L, path("path"));
    doc.set("f1", "val1");

    Long id = doc.getId();
    when(repository.deleteById(id)).thenReturn(Mono.empty());
    when(repository.findById(id)).thenReturn(Mono.just(doc));
    Path path = path("a", "b", "c", "" + id);
    service.delete(path)
      .block();

    verify(publisher, times(1)).publishDocumentDeleted(doc);
  }

}

package io.gitlab.restbase.server.message;

import static io.gitlab.restbase.message.ChildEventType.ADDED;
import static io.gitlab.restbase.message.ChildEventType.REMOVED;
import static io.gitlab.restbase.message.TopicEncoder.CHANGES_SUFFIX;
import static io.gitlab.restbase.message.TopicEncoder.CHILDREN_SUFFIX;
import static io.gitlab.restbase.message.TopicEncoder.encode;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.PathUtils;
import io.gitlab.restbase.message.CollectionChildEvent;
import io.gitlab.restbase.message.DocumentChildEvent;

@Service
public class Publisher {

  public static final String STOMP_TOPIC_EXCHANGE = "amq.topic";

  final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private AmqpTemplate amqpTemplate;

  public void publishDocumentUpdated(DocumentWithId doc) {
    Path path = doc.path();
    String routingKey = changesTopic(path);
    DocumentWithId event = doc;
    logger.info("documentUpdated: {} -> {}", routingKey, doc);
    amqpTemplate.convertAndSend(STOMP_TOPIC_EXCHANGE, routingKey, event);
  }

  public void publishDocumentAdded(DocumentWithId doc) {
    Path parentPath = doc.path()
      .parent();
    String routingKey = childrenTopic(parentPath);
    CollectionChildEvent event = new CollectionChildEvent(parentPath, doc, ADDED);
    logger.info("documentAdded: {} -> {}", routingKey, doc);
    amqpTemplate.convertAndSend(STOMP_TOPIC_EXCHANGE, routingKey, event);
  }

  public void publishDocumentDeleted(DocumentWithId doc) {
    Path parentPath = doc.path()
      .parent();
    String routingKey = childrenTopic(parentPath);
    CollectionChildEvent event = new CollectionChildEvent(parentPath, doc, REMOVED);
    logger.info("documentDeleted: {} -> {}", routingKey, doc);
    amqpTemplate.convertAndSend(STOMP_TOPIC_EXCHANGE, routingKey, event);
  }

  public void publishCollectionAdded(Collection collection) {
    Path parentPath = collection.path()
      .parent();
    String routingKey = childrenTopic(parentPath);
    DocumentChildEvent event = new DocumentChildEvent(parentPath, collection.getName(), ADDED);
    logger.info("collectionAdded: {} -> {}", routingKey, collection);
    amqpTemplate.convertAndSend(STOMP_TOPIC_EXCHANGE, routingKey, event);
  }

  public void publishCollectionDeleted(String document, String collection) {
    Path parentPath = PathUtils.path(document);
    String routingKey = childrenTopic(parentPath);
    DocumentChildEvent event = new DocumentChildEvent(parentPath, collection, REMOVED);
    logger.info("collectionDeleted: {} -> {}", routingKey, collection);
    amqpTemplate.convertAndSend(STOMP_TOPIC_EXCHANGE, routingKey, event);
  }

  private String childrenTopic(Path path) {
    if (path == null || path.isRoot())
      return ".children";
    String fullPath = MessageFormat.format("{0}{1}", path.joined(), CHILDREN_SUFFIX);
    return encode(fullPath);
  }

  private String changesTopic(Path path) {
    if (path == null || path.isRoot())
      return ".changes";
    String fullPath = MessageFormat.format("{0}{1}", path.joined(), CHANGES_SUFFIX);
    return encode(fullPath);
  }
}

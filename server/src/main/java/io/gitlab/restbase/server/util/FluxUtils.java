package io.gitlab.restbase.server.util;

import io.gitlab.restbase.server.exception.NotFoundException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxUtils {

  private FluxUtils() {
  }

  public static <T> Mono<T> wrapNotFoundIfEmpty(Mono<T> finder, String entity, String identifier) {
    return finder.switchIfEmpty(Mono.defer(() -> {
      return Mono.error(new NotFoundException(entity, identifier));
    }));
  }

  public static <T> Flux<T> wrapNotFoundIfEmpty(Flux<T> finder, String entity, String identifier) {
    return finder.switchIfEmpty(Mono.defer(() -> {
      return Mono.error(new NotFoundException(entity, identifier));
    }));
  }

  public static <T> Mono<T> deferError(Throwable error) {
    return Mono.defer(() -> {
      return Mono.error(error);
    });
  }

  public static <T> Flux<T> deferErrorFlux(Throwable error) {
    return Flux.defer(() -> {
      return Flux.error(error);
    });
  }
}

package io.gitlab.restbase.server;

import static io.gitlab.restbase.server.DocumentService.DOCUMENT_ROOT_ID;
import static io.gitlab.restbase.server.util.FluxUtils.deferError;
import static io.gitlab.restbase.server.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.server.exception.NotFoundException;
import io.gitlab.restbase.server.message.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CollectionService {

  final Logger logger = LoggerFactory.getLogger(getClass());

  private final CollectionRepository collections;
  private final Publisher publisher;

  public CollectionService(CollectionRepository collections, Publisher publisher) {
    this.collections = collections;
    this.publisher = publisher;
  }

  public Mono<Collection> get(Path path) {
    String document = path.parent()
      .joined();
    String collection = path.collectionName()
      .get();
    return wrapNotFoundIfEmpty(collections.findByDocumentAndName(document, collection), "document", path.toString());
  }

  public Mono<Collection> getAtRoot(String name) {
    return wrapNotFoundIfEmpty(collections.findByDocumentAndName(DOCUMENT_ROOT_ID, name), "collection", name);
  }

  public Mono<Collection> delete(Path path) {
    if (path.isCollection()) {
      Path parent = path.parent();
      String document = parent.isRoot() ? DOCUMENT_ROOT_ID : parent.joined();
      String collection = path.collectionName()
        .get();
      return collections.findByDocumentAndName(document, collection)
        .flatMap(this::doDeleteCollection)
        .doOnSuccess(col -> publisher.publishCollectionDeleted(document, collection));
    }
    return deferError(new NotFoundException("collection", path.toString()));
  }

  public Mono<Collection> deleteAtRoot(String name) {
    String document = DOCUMENT_ROOT_ID;
    return collections.findByDocumentAndName(document, name)
      .flatMap(this::doDeleteCollection)
      .doOnSuccess(col -> {
        publisher.publishCollectionDeleted(document, name);
      });
  }

  private Mono<Collection> doDeleteCollection(Collection c) {
    String document = c.getDocument();
    String name = c.getName();
    logger.info("deleting at '{}': '{}'", document, name);
    return collections.deleteByDocumentAndName(document, name)
      .thenReturn(c);
  }

  public Mono<Collection> save(Path path, @Valid Collection collection) {
    if (path.isDocument()) {
      String document = path.isRoot() ? DOCUMENT_ROOT_ID : path.joined();
      collection.setDocument(document);
      return collections.save(collection)
        .doOnNext(col -> publisher.publishCollectionAdded(col));
    }
    return deferError(new NotFoundException("document", path.toString()));
  }

  public Flux<Collection> listAll(Path path) {
    Flux<Collection> collection = collections.findByDocument(path.joined());
    return wrapNotFoundIfEmpty(collection, "collection", path.toString());
  }

  public Flux<Collection> listAllAtRoot() {
    return collections.findByDocument(DOCUMENT_ROOT_ID);
  }

}

package io.gitlab.restbase.server;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.PathUtils.root;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class DocumentController {

  DocumentService service;;

  @Autowired
  private DocumentController(DocumentService service) {
    this.service = service;
  }

  @GetMapping()
  public Mono<DocumentWithId> getRoot() {
    return service.getRoot();
  }

  @GetMapping("/{p1}/{p2}")
  public Mono<DocumentWithId> get2(@PathVariable String p1, @PathVariable String p2) {
    return service.get(path(p1, p2));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}")
  public Mono<DocumentWithId> get3(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4) {
    return service.get(path(p1, p2, p3, p4));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}")
  public Mono<DocumentWithId> get4(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6) {
    return service.get(path(p1, p2, p3, p4, p5, p6));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}")
  public Mono<DocumentWithId> get5(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7,
      @PathVariable String p8) {
    return service.get(path(p1, p2, p3, p4, p5, p6, p7, p8));
  }

  @PutMapping()
  public Mono<DocumentWithId> putRoot(@Valid @RequestBody DocumentWithId document) {
    return service.update(root(), document);
  }

  @PutMapping("/{p1}/{p2}")
  public Mono<DocumentWithId> put2(@Valid @RequestBody DocumentWithId document, @PathVariable String p1,
      @PathVariable String p2) {
    return service.update(path(p1, p2), document);
  }

  @PutMapping("/{p1}/{p2}/{p3}/{p4}")
  public Mono<DocumentWithId> put3(@Valid @RequestBody DocumentWithId document, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4) {
    return service.update(path(p1, p2, p3, p4), document);
  }

  @PutMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}")
  public Mono<DocumentWithId> put4(@Valid @RequestBody DocumentWithId document, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4, @PathVariable String p5,
      @PathVariable String p6) {
    return service.update(path(p1, p2, p3, p4, p5, p6), document);
  }

  @PutMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}")
  public Mono<DocumentWithId> put5(@Valid @RequestBody DocumentWithId document, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4, @PathVariable String p5,
      @PathVariable String p6, @PathVariable String p7, @PathVariable String p8) {
    return service.update(path(p1, p2, p3, p4, p5, p6, p7, p8), document);
  }

  @PostMapping("/{p1}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<DocumentWithId> create1(@RequestBody @Valid Document document, @PathVariable String p1) {
    return service.save(path(p1), document);
  }

  @PostMapping("/{p1}/{p2}/{p3}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<DocumentWithId> create2(@Valid @RequestBody Document document, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3) {
    return service.save(path(p1, p2, p3), document);
  }

  @PostMapping("/{p1}/{p2}/{p3}/{p4}/{p5}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<DocumentWithId> create3(@Valid @RequestBody Document document, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4, @PathVariable String p5) {
    return service.save(path(p1, p2, p3, p4, p5), document);
  }

  @PostMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<DocumentWithId> create4(@Valid @RequestBody Document document, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4, @PathVariable String p5,
      @PathVariable String p6, @PathVariable String p7) {
    return service.save(path(p1, p2, p3, p4, p5, p6, p7), document);
  }

  @DeleteMapping("/{p1}/{p2}")
  public Mono<DocumentWithId> delete1(@PathVariable String p1, @PathVariable String p2) {
    return service.delete(path(p1, p2));
  }

  @DeleteMapping("/{p1}/{p2}/{p3}/{p4}")
  public Mono<DocumentWithId> delete2(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4) {
    return service.delete(path(p1, p2, p3, p4));
  }

  @DeleteMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}")
  public Mono<DocumentWithId> delete3(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6) {
    return service.delete(path(p1, p2, p3, p4, p5, p6));
  }

  @DeleteMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}")
  public Mono<DocumentWithId> delete4(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7,
      @PathVariable String p8) {
    return service.delete(path(p1, p2, p3, p4, p5, p6, p7, p8));
  }

  @GetMapping("/{p1}/documents")
  public Flux<DocumentWithId> documents1(@PathVariable String p1) {
    return service.getAll(path(p1));
  }

  @GetMapping("/{p1}/{p2}/{p3}/documents")
  public Flux<DocumentWithId> documents2(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3) {
    return service.getAll(path(p1, p2, p3));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/documents")
  public Flux<DocumentWithId> documents3(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5) {
    return service.getAll(path(p1, p2, p3, p4, p5));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/documents")
  public Flux<DocumentWithId> documents4(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7) {
    return service.getAll(path(p1, p2, p3, p4, p5, p6, p7));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}/{p9}/documents")
  public Flux<DocumentWithId> documents5(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7,
      @PathVariable String p8, @PathVariable String p9) {
    return service.getAll(path(p1, p2, p3, p4, p5, p6, p7, p8, p9));
  }

}

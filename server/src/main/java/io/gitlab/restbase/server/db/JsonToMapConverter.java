package io.gitlab.restbase.server.db;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.r2dbc.postgresql.codec.Json;

@ReadingConverter
public class JsonToMapConverter implements Converter<Json, Map<String, Object>> {

  private ObjectMapper objectMapper;

  public JsonToMapConverter(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public Map<String, Object> convert(Json source) {
    byte[] sourcebytes = source.asArray();
    try {
      TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
      };
      HashMap<String, Object> value = objectMapper.readValue(sourcebytes, typeRef);
      return value;
    } catch (IOException e) {
      throw new JsonSerializationException();
    }
  }

}

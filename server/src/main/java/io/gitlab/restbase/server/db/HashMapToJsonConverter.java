package io.gitlab.restbase.server.db;

import java.util.HashMap;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.r2dbc.postgresql.codec.Json;

@WritingConverter
public class HashMapToJsonConverter implements Converter<HashMap<String, Object>, Json> {

  private ObjectMapper objectMapper;

  public HashMapToJsonConverter(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public Json convert(HashMap<String, Object> source) {
    try {
      String valueAsString = objectMapper.writeValueAsString(source);
      return Json.of(valueAsString);
    } catch (JsonProcessingException e) {
      throw new JsonSerializationException();
    }
  }

}

package io.gitlab.restbase.server;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import io.gitlab.restbase.Collection;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface CollectionRepository extends ReactiveCrudRepository<Collection, Long> {

  @Query("SELECT * FROM collection WHERE document = :document AND name = :name")
  Mono<Collection> findByDocumentAndName(@Nullable String document, String name);

  @Query("SELECT COUNT (*) FROM collection WHERE document = :document AND name = :name")
  Mono<Long> countByDocumentAndName(@Nullable String document, String name);

  @Query("DELETE FROM collection WHERE document = :document AND name = :name")
  Mono<Void> deleteByDocumentAndName(@Nullable String document, String name);

  @Query("SELECT * FROM collection WHERE document = :document")
  Flux<Collection> findByDocument(@Nullable String document);

}

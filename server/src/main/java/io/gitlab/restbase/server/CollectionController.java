package io.gitlab.restbase.server;

import static io.gitlab.restbase.PathUtils.path;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Path;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class CollectionController {

  private CollectionService service;

  @Autowired
  public CollectionController(CollectionService service) {
    this.service = service;
  }

  @GetMapping("/{p1}")
  public Mono<Collection> get1(@PathVariable String p1) {
    return service.getAtRoot(p1);
  }

  @GetMapping("/{p1}/{p2}/{p3}")
  public Mono<Collection> get2(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3) {
    return service.get(path(p1, p2, p3));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}")
  public Mono<Collection> get3(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5) {
    return service.get(path(p1, p2, p3, p4, p5));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}")
  public Mono<Collection> get4(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7) {
    return service.get(path(p1, p2, p3, p4, p5, p6, p7));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}/{p9}")
  public Mono<Collection> get5(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7,
      @PathVariable String p8, @PathVariable String p9) {
    return service.get(path(p1, p2, p3, p4, p5, p6, p7, p8, p9));
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Collection> create1(@Valid @RequestBody Collection collection) {
    return service.save(Path.ROOT, collection);
  }

  @PostMapping("/{p1}/{p2}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Collection> create2(@Valid @RequestBody Collection collection, @PathVariable String p1,
      @PathVariable String p2) {
    return service.save(path(p1, p2), collection);
  }

  @PostMapping("/{p1}/{p2}/{p3}/{p4}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Collection> create3(@Valid @RequestBody Collection collection, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4) {
    return service.save(path(p1, p2, p3, p4), collection);
  }

  @PostMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Collection> create4(@Valid @RequestBody Collection collection, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4, @PathVariable String p5,
      @PathVariable String p6) {
    return service.save(path(p1, p2, p3, p4, p5, p6), collection);
  }

  @PostMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Collection> create5(@Valid @RequestBody Collection collection, @PathVariable String p1,
      @PathVariable String p2, @PathVariable String p3, @PathVariable String p4, @PathVariable String p5,
      @PathVariable String p6, @PathVariable String p7, @PathVariable String p8) {
    return service.save(path(p1, p2, p3, p4, p5, p6, p7, p8), collection);
  }

  @DeleteMapping("/{p1}")
  public Mono<Collection> delete1(@PathVariable String p1) {
    return service.deleteAtRoot(p1);
  }

  @DeleteMapping("/{p1}/{p2}/{p3}")
  public Mono<Collection> delete2(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3) {
    return service.delete(path(p1, p2, p3));
  }

  @DeleteMapping("/{p1}/{p2}/{p3}/{p4}/{p5}")
  public Mono<Collection> delete3(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5) {
    return service.delete(path(p1, p2, p3, p4, p5));
  }

  @DeleteMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}")
  public Mono<Collection> delete4(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7) {
    return service.delete(path(p1, p2, p3, p4, p5, p6, p7));
  }

  @DeleteMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}/{p9}")
  public Mono<Collection> delete5(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7,
      @PathVariable String p8, @PathVariable String p9) {
    return service.delete(path(p1, p2, p3, p4, p5, p6, p7, p8, p9));
  }

  @GetMapping("/collections")
  public Flux<Collection> getRootCollections() {
    return service.listAllAtRoot();
  }

  @GetMapping("/{p1}/{p2}/collections")
  public Flux<Collection> collections2(@PathVariable String p1, @PathVariable String p2) {
    return service.listAll(path(p1, p2));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/collections")
  public Flux<Collection> collections3(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4) {
    return service.listAll(path(p1, p2, p3, p4));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/collections")
  public Flux<Collection> collections4(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6) {
    return service.listAll(path(p1, p2, p3, p4, p5, p6));
  }

  @GetMapping("/{p1}/{p2}/{p3}/{p4}/{p5}/{p6}/{p7}/{p8}/collections")
  public Flux<Collection> collections5(@PathVariable String p1, @PathVariable String p2, @PathVariable String p3,
      @PathVariable String p4, @PathVariable String p5, @PathVariable String p6, @PathVariable String p7,
      @PathVariable String p8) {
    return service.listAll(path(p1, p2, p3, p4, p5, p6, p7, p8));
  }

}

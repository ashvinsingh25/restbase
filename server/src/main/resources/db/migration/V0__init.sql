CREATE TABLE IF NOT EXISTS "collection" (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    document VARCHAR
);

CREATE TABLE IF NOT EXISTS "document" (
    id serial PRIMARY KEY,
    collection VARCHAR,
    fields JSONB
);

INSERT INTO "document" (id) VALUES (-1) ON CONFLICT DO NOTHING

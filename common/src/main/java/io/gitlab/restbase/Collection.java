package io.gitlab.restbase;


import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Objects;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table("collection")
public class Collection implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private Long id;

  @JsonProperty
  @NotBlank
  private String name;

  @JsonProperty
  @Nullable
  private String document;

  public Collection(Path parent, String name) {
    if (parent == null)
      throw new IllegalArgumentException("Parent path cannot be null");
    this.name = name;
    this.document = parent.joined();
  }

  public Collection() {
  }

  public Collection(String name) {
    this(Path.ROOT, name);
  }

  public Long getId() {
    return id;
  }

  @JsonIgnore
  public Path getParent() {
    return PathUtils.path(document);
  }

  public Path path() {
    return getParent().append(name);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDocument() {
    return document;
  }

  public void setDocument(String document) {
    this.document = document;
  }

  @Override
  public String toString() {
    return MessageFormat.format("Collection [{0}]", path().joined());
  }

  @Override
  public int hashCode() {
    return Objects.hash(path());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Collection other = (Collection) obj;
    if (path() == null) {
      if (other.path() != null)
        return false;
    } else if (!path().equals(other.path()))
      return false;
    return true;
  }

}

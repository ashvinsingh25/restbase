package io.gitlab.restbase;

import static io.gitlab.restbase.Document.ROOT_ID;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonDeserialize(using = PathDeserializer.class)
@JsonSerialize(using = PathSerializer.class)
public class Path implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final Path ROOT = new Path();

  private List<String> segments;

  public Path() {
    this(new String[] {});
  }

  @JsonCreator
  public Path(@JsonProperty("segments") String... segments) {
    this(Arrays.asList(segments));
  }

  public Path(List<String> segments) {
    if (segments.stream()
      .anyMatch(s -> null == s || "".contentEquals(s)))
      throw new IllegalArgumentException("path segments cannot be null or empty");
    this.segments = segments;
  }

  @Override
  public String toString() {
    return segments.toString();
  }

  public String joined() {
    return segments.stream()
      .collect(Collectors.joining("/"));
  }

  @JsonProperty
  public List<String> getSegments() {
    return segments;
  }

  @JsonIgnore
  public boolean isRoot() {
    return segments.isEmpty() || (segments.size() == 1 && ("" + ROOT_ID).equals(segments.get(0)));
  }

  @JsonIgnore
  public boolean isDocument() {
    return segments.size() % 2 == 0;
  }

  @JsonIgnore
  public boolean isCollection() {
    return segments.size() % 2 != 0;
  }

  public Optional<String> documentKey() {
    if (!isDocument() || segments.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(segments.get(segments.size() - 1));
  }

  public Optional<String> collectionName() {
    if (!isCollection() || segments.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(segments.get(segments.size() - 1));
  }

  public Path parent() {
    if (segments.size() < 2) {
      return ROOT;
    }
    List<String> parentSegments = new ArrayList<>(segments);
    parentSegments.remove(parentSegments.size() - 1);
    return new Path(parentSegments.stream()
      .collect(Collectors.toList()));
  }

  public Path append(String name) {
    List<String> list = new ArrayList<>(segments);
    list.add(name);
    return new Path(list);
  }

  public Path append(Long id) {
    List<String> list = new ArrayList<>(segments);
    list.add(id + "");
    return new Path(list);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(segments);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Path other = (Path) obj;
    if (this.isRoot() && other.isRoot())
      return true;
    return Objects.equals(segments, other.segments);
  }

}

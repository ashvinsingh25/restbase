package io.gitlab.restbase;

public class PathUtils {

  private PathUtils() {
  }

  public static Path path(String... segments) {
    return new Path(segments);
  }

  public static Path path(String path) {
    if (isRoot(path))
      return Path.ROOT;
    return new Path(path.split("\\/"));
  }

  public static Path root() {
    return Path.ROOT;
  }

  public static boolean isRoot(String string) {
    return string == null || "".equals(string);
  }
}

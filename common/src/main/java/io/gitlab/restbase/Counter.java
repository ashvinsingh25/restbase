package io.gitlab.restbase;

public class Counter {

  private int count;

  Counter(int i) {
    this.count = i;
  }

  public int inc() {
    return ++count;
  }

  public int value() {
    return count;
  }

  public static Counter startWith(int initial) {
    return new Counter(initial);
  }

}

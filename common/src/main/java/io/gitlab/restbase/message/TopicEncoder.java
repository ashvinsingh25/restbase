package io.gitlab.restbase.message;

import static io.gitlab.restbase.PathUtils.path;

import java.text.MessageFormat;
import java.util.Arrays;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;

public class TopicEncoder {

  public static final String CHILDREN_SUFFIX = ".children";
  public static final String CHANGES_SUFFIX = ".changes";
  
  private static final String PATH_SEPARATOR = "@@@@";
  public static final String DESTINATION_PREFIX = "/topic";

  public static String changesTopic(DocumentWithId document) {
    return changesTopic(document.path());
  }

  public static String childrenTopic(DocumentWithId document) {
    return childrenTopic(document.path());
  }

  public static String childrenTopic(Collection collection) {
    return childrenTopic(collection.path());
  }

  public static String changesTopic(Path path) {
    if (path == null)
      return null;
    return MessageFormat.format("{0}/{1}{2}", DESTINATION_PREFIX, encode(path), CHANGES_SUFFIX);
  }

  public static String childrenTopic(Path path) {
    if (path == null)
      return null;
    return MessageFormat.format("{0}/{1}{2}", DESTINATION_PREFIX, encode(path), CHILDREN_SUFFIX);
  }

  public static String childrenTopic(String... pathSegments) {
    if (pathSegments == null)
      return null;
    if (Arrays.equals(pathSegments, new String[] { "" })) {
      return childrenTopic(Path.ROOT);
    }
    return childrenTopic(path(pathSegments));
  }

  private static String encode(Path path) {
    return encode(path.joined());
  }

  public static String encode(String string) {
    if (string == null)
      return null;
    return string.replace("/", PATH_SEPARATOR);
  }

}

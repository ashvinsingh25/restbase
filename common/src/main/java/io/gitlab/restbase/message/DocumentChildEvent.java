package io.gitlab.restbase.message;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Objects;

import io.gitlab.restbase.Path;

public class DocumentChildEvent implements Serializable, ChildEvent {

  private static final long serialVersionUID = 1L;

  private Path parent;
  private String collection;
  private ChildEventType eventType;

  public DocumentChildEvent() {
  }

  public DocumentChildEvent(Path parent, String collection, ChildEventType eventType) {
    this.parent = parent;
    this.collection = collection;
    this.eventType = eventType;
  }

  public Path getParent() {
    return parent;
  }

  public String getCollection() {
    return collection;
  }

  public ChildEventType getEventType() {
    return eventType;
  }

  @Override
  public String toString() {
    return MessageFormat.format("DocumentChildEvent [parent={0}, collection={1}, eventType={2}]", parent, collection, eventType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(collection, parent, eventType);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DocumentChildEvent other = (DocumentChildEvent) obj;
    return Objects.equals(collection, other.collection) 
        && Objects.equals(parent, other.parent)
        && Objects.equals(eventType, other.eventType);
  }

}

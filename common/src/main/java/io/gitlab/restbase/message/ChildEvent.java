package io.gitlab.restbase.message;

import io.gitlab.restbase.Path;

public interface ChildEvent {

  ChildEventType getEventType();

  Path getParent();

}

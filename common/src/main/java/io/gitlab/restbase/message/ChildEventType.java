package io.gitlab.restbase.message;

public enum ChildEventType {

  ADDED,

  REMOVED,

}

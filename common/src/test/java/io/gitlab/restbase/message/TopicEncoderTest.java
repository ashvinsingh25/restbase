package io.gitlab.restbase.message;

import static io.gitlab.restbase.message.TopicEncoder.changesTopic;
import static io.gitlab.restbase.message.TopicEncoder.childrenTopic;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;

public class TopicEncoderTest {

  @Test(timeout = 5000)
  public void changesTopicNullPath() {
    assertNull(changesTopic((Path) null));
  }
  
  @Test(timeout = 5000)
  public void childrenTopicNullPath() {
    assertNull(childrenTopic((Path) null));
  }
  
  @Test(timeout = 5000)
  public void changesTopicRootDocument() {
    assertEquals("/topic/.changes", changesTopic(DocumentWithId.ROOT));
  }
  
  @Test(timeout = 5000)
  public void childrenTopicRootDocument() {
    assertEquals("/topic/.children", childrenTopic(DocumentWithId.ROOT));
  }
  
  @Test(timeout = 5000)
  public void childrenTopicString() {
    assertEquals("/topic/.children", childrenTopic(""));
    assertEquals("/topic/abc.children", childrenTopic("abc"));
    assertEquals("/topic/abc@@@@def.children", childrenTopic("abc/def"));
    assertEquals("/topic/abc@@@@def@@@@123.children", childrenTopic("abc/def/123"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void childrenTopicNullStringThrowsException() {
    assertEquals(null, childrenTopic((String) null));
  }

}

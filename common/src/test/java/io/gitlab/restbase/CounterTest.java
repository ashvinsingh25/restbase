package io.gitlab.restbase;

import static org.junit.Assert.*;

import org.junit.Test;

public class CounterTest {

  @Test
  public void inc() {
    Counter c = Counter.startWith(42);
    assertEquals(43, c.inc());
  }

  @Test
  public void startWith() {
    Counter c = Counter.startWith(42);
    assertNotNull(c);
    assertEquals(42, c.value());
  }

}

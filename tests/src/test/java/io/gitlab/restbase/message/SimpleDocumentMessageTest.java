package io.gitlab.restbase.message;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.CollectionOperations;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.server.RestbaseServer;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { RestbaseServer.class,
    WebSocketConfiguration.class, WebsocketProperties.class })

public class SimpleDocumentMessageTest {

  static final String API_KEY = "testApiKey";

  Restbase restbase;
  StompSession stompSession;

  @Value("${local.server.port}")
  private int port;

  @Before
  public void init() {
    String rootUrl = "http://localhost:" + port;
    RestBaseConfiguration config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    stompSession = restbase.getStompSession();
  }

  @Test(timeout = 10000)
  public void addDocument() {

    CollectionOperations collection = restbase.collection("collection1");

    StepVerifier.create(collection.children())
      .then(() -> {
        collection.add(document().with("string1", "test")
          .with("number1", 123))
          .block();
      })
      .expectNextMatches(p -> path("collection1").equals(p.getParent()) && ChildEventType.ADDED == p.getEventType())
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 10000)
  public void deleteDocument() {

    CollectionOperations collection = restbase.collection("collection2");
    DocumentWithId document = collection.add(document().with("string1", "test")
      .with("number1", 123))
      .block();
    Long id = document.getId();

    StepVerifier.create(collection.children())
      .then(() -> restbase.delete(document)
        .block())
      .expectNextMatches(p -> path("collection2").equals(p.getParent()) && ChildEventType.REMOVED == p.getEventType()
          && id.equals(p.getDocument()
            .getId()))
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 10000)
  public void updateDocument() {

    DocumentWithId document = restbase.collection("collection2")
      .add(document().with("string1", "test"))
      .block();

    StepVerifier.create(restbase.changes(document.path()))
      .then(() -> {
        document.set("f", 42);
        restbase.update(document)
          .block();
      })
      .expectNextMatches(p -> document.path()
        .equals(p.path())
          && Optional.of(42)
            .equals(document.get("f")))
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

}

package io.gitlab.restbase.message;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;
import static java.util.Optional.of;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.CollectionOperations;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.server.RestbaseServer;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { RestbaseServer.class,
    WebSocketConfiguration.class, WebsocketProperties.class })

public class CollectionMessageTest {

  static final String API_KEY = "testApiKey";

  Restbase restbase;
  StompSession stompSession;

  @Value("${local.server.port}")
  private int port;

  @Before
  public void init() {
    String rootUrl = "http://localhost:" + port;
    RestBaseConfiguration config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    stompSession = restbase.getStompSession();
  }

  @Test(timeout = 10000)
  public void addDocument() {
    CollectionOperations collectionRef = restbase.collection("collection1");

    StepVerifier.create(collectionRef.children())
      .then(() -> {
        collectionRef.add(document().with("test", "value"))
          .block();
      })
      .expectNextMatches(p -> path("collection1").equals(p.getParent()) && ChildEventType.ADDED == p.getEventType()
          && p.getDocument() != null && of("value").equals(p.getDocument()
            .get("test")))
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 10000)
  public void removeDocument() {
    CollectionOperations collectionRef = restbase.collection("collection1");
    DocumentWithId document = collectionRef.add(document().with("test", "value"))
      .block();

    StepVerifier.create(collectionRef.children())
      .then(() -> {
        restbase.delete(document)
          .block();
      })
      .expectNextMatches(p -> path("collection1").equals(p.getParent()) && ChildEventType.REMOVED == p.getEventType()
          && p.getDocument() != null && of("value").equals(p.getDocument()
            .get("test")))
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

}

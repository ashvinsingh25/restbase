package io.gitlab.restbase;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.restbase;
import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;

public class ClientServerTest {

  static final String ROOT_URL = "http://localhost:8080";
  static final String API_KEY = "testApiKey";

  Restbase restbase;

  @ClassRule
  public static final RestbaseServerRule server = new RestbaseServerRule();

  @Before
  public void init() {
    RestBaseConfiguration config = config(ROOT_URL, API_KEY);
    restbase = restbase(config);
  }

  @Test
  public void clientConnects() throws Exception {
    assertTrue(restbase.online()
      .block(Duration.ofSeconds(3)));
  }
}

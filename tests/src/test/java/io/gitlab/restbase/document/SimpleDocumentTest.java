package io.gitlab.restbase.document;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.RestbaseServerRule;
import io.gitlab.restbase.client.DocumentNotFoundException;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;

public class SimpleDocumentTest {

  static final String ID = "id";

  static final String ROOT_URL = "http://localhost:8080";
  static final String API_KEY = "testApiKey";

  Restbase restbase;

  ObjectMapper objectMapper;

  @ClassRule
  public static final RestbaseServerRule server = new RestbaseServerRule();

  @Before
  public void init() {
    RestBaseConfiguration config = config(ROOT_URL, API_KEY);
    restbase = restbase(config);
    objectMapper = restbase.getObjectMapper();
  }

  @Test
  public void addDocument() throws InterruptedException, ExecutionException, JsonProcessingException {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test")
        .with("number1", 123))
      .block();

    assertNotNull(document);
    assertEquals(of("test"), document.get("string1"));
    assertEquals(of(123), document.get("number1"));
  }

  @Test(expected = DocumentNotFoundException.class)
  public void deleteDocument() throws Throwable {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test")
        .with("number1", 123))
      .block();
    Long id = document.getId();
    DocumentWithId deleted = restbase.delete(document)
      .block();
    assertNotNull(deleted);
    assertEquals(id, deleted.getId());

    restbase.collection("collection1")
      .document(id)
      .block();
  }

  @Test
  public void updateDocument() {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test"))
      .block();
    document.set("number1", 123);

    DocumentWithId updated = restbase.update(document)
      .block();
    assertNotNull(updated);
    assertEquals(Optional.of(123), updated.get("number1"));
  }

  @Test(expected = DocumentNotFoundException.class)
  public void deleteMissingDocument() throws Throwable {
    ImmutableMap<String, Object> doc = ImmutableMap.of(ID, -123, "collection", "missingcollection", "string1", "test",
        "number1", 123);
    String docAsString = objectMapper.writeValueAsString(doc);
    DocumentWithId document = objectMapper.readValue(docAsString, DocumentWithId.class);

    DocumentWithId deleted = restbase.delete(document)
      .block();
    assertNull(deleted);
  }

  @Test(expected = DocumentNotFoundException.class)
  public void getMissingDocument() throws Throwable {
    restbase.collection("collection1")
      .document(-123L)
      .block();
  }

}

package io.gitlab.restbase;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import io.gitlab.restbase.server.RestbaseServer;

public class RestbaseServerRule extends TestWatcher {

  private String[] arguments;
  private SpringApplication application;
  private ConfigurableApplicationContext applicationContext;

  public RestbaseServerRule() {
    this(RestbaseServer.class, new String[] {});
  }

  public RestbaseServerRule(Class<RestbaseServer> applicationClass, String[] arguments) {
    this.arguments = arguments;
    this.application = new SpringApplication(applicationClass);
  }

  @Override
  protected void starting(Description description) {
    applicationContext = application.run(this.arguments);
  }

  @Override
  protected void finished(Description description) {
    if (applicationContext != null) {
      applicationContext.close();
    }
  }

}

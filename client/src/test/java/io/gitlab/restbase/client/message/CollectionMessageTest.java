package io.gitlab.restbase.client.message;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.restbase;
import static io.gitlab.restbase.message.TopicEncoder.childrenTopic;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.CollectionOperations;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.message.ChildEventType;
import io.gitlab.restbase.message.CollectionChildEvent;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SimpleBrokerTestApplication.class)
public class CollectionMessageTest {

  static String API_KEY = "testApiKey";

  private Restbase restbase;

  @Value("${local.server.port}")
  private int port;

  private StompSession stompSession;

  private DocumentWithId savedDocument;

  @Before
  public void setup() {
    String rootUrl = "http://localhost:" + port;
    restbase = restbase(config(rootUrl, API_KEY));
    stompSession = restbase.getStompSession();
  }

  @Test(timeout = 5000)
  public void addChildAndDisconnect() {
    CollectionOperations collectionRef = restbase.collection("collection1");
    CollectionChildEvent added = new CollectionChildEvent(path("collection1"), savedDocument, ChildEventType.ADDED);

    StepVerifier.create(collectionRef.children())
      .then(() -> {
        stompSession.send(childrenTopic("collection1"), added);
      })
      .expectNext(added)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 5000)
  public void removeChildAndDisconnect() {
    CollectionOperations collectionRef = restbase.collection("collection1");
    CollectionChildEvent removed = new CollectionChildEvent(path("collection1"), savedDocument, ChildEventType.REMOVED);

    StepVerifier.create(collectionRef.children())
      .then(() -> {
        stompSession.send(childrenTopic("collection1"), removed);
      })
      .expectNext(removed)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

}

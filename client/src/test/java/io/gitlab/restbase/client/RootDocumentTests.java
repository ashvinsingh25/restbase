package io.gitlab.restbase.client;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.restbase;
import static io.gitlab.restbase.test.utils.MockResponseBuilder.response;
import static io.gitlab.restbase.test.utils.TestUrlUtils.url;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.CollectionNotFoundException;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

public class RootDocumentTests {

  static final String NAME = "name";
  static final String ID = "id";
  static final String DEFAULT_ID = "123";
  static final String COLLECTION = "collection";

  static String API_KEY = "testApiKey";

  private RestBaseConfiguration config;
  private Restbase restbase;
  private ObjectMapper objectMapper;
  private MockWebServer server;
  private String rootUrl;
  private String apiUrl;

  @Before
  public void setup() {
    server = new MockWebServer();
    rootUrl = server.url("/") + "";
    apiUrl = rootUrl + "api";
    config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    objectMapper = restbase.getObjectMapper();
  }

  @After
  public void shutdown() throws Exception {
    server.shutdown();
  }

  @Test
  public void initialize() {
    assertNotNull(restbase);
  }

  @Test(timeout = 5000)
  public void addCollection() throws InterruptedException, ExecutionException, IOException {
    ImmutableMap<String, Object> col = ImmutableMap.of(NAME, "collection1");

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, col)
      .enqueue();

    Collection collection = restbase.collection("collection1")
      .add()
      .block();

    assertNotNull(collection);
    assertEquals("collection1", collection.getName());

    assertEquals(1, server.getRequestCount());

    RecordedRequest request = server.takeRequest();
    assertEquals(apiUrl, request.getRequestUrl()
      .toString());
    assertEquals("POST", request.getMethod());

  }

  @Test(timeout = 5000)
  public void updateRootFields() throws InterruptedException, ExecutionException, IOException {
    ImmutableMap<String, Object> fields = ImmutableMap.of(ID, DEFAULT_ID, "string1", "test",
        "number1", 123);

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, fields)
      .enqueue();

    DocumentWithId root = DocumentWithId.ROOT;
    root.set("string1", "test");
    DocumentWithId updated = restbase.update(root)
      .block();

    assertNotNull(updated);
    assertEquals(Optional.of("test"), updated.get("string1"));

    assertEquals(1, server.getRequestCount());

    RecordedRequest request = server.takeRequest();
    assertEquals(apiUrl, request.getRequestUrl()
      .toString());
    assertEquals("PUT", request.getMethod());
  }

  @Test(timeout = 5000)
  public void loadCollection() throws Throwable {
    ImmutableMap<String, Object> col = ImmutableMap.of(NAME, "collection1");

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, col)
      .enqueue();

    Collection collection = restbase.collection("collection1")
      .load()
      .block();

    assertNotNull(collection);
    assertEquals("collection1", collection.getName());

    assertEquals(1, server.getRequestCount());
    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1"), request.getRequestUrl());
    assertEquals("GET", request.getMethod());
  }

  @Test(timeout = 5000)
  public void listCollections() throws Throwable {
    List<Map<String, Object>> cols = Arrays.asList(ImmutableMap.of(NAME, "collection1"),
        ImmutableMap.of(NAME, "collection2"));

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, cols)
      .enqueue();

    List<Collection> collections = restbase.collections()
      .collectList()
      .block();

    assertNotNull(collections);
    assertEquals(2, collections.size());

    assertEquals(1, server.getRequestCount());
    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "collections"), request.getRequestUrl());
    assertEquals("GET", request.getMethod());
  }

  @Test(timeout = 5000)
  public void deleteCollection() throws Throwable {
    ImmutableMap<String, Object> col = ImmutableMap.of(NAME, "collection1");

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, col)
      .enqueue();

    restbase.collection("collection1")
      .delete()
      .block();

    assertEquals(1, server.getRequestCount());
    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1"), request.getRequestUrl());
    assertEquals("DELETE", request.getMethod());
  }

  @Test(timeout = 5000, expected = CollectionNotFoundException.class)
  public void deleteMissingCollection() throws Throwable {

    response().on(server)
      .withStatus(HttpStatus.NOT_FOUND)
      .enqueue();

    restbase.collection("collection1")
      .delete()
      .block();

    assertEquals(1, server.getRequestCount());
  }

  @Test(timeout = 5000, expected = CollectionNotFoundException.class)
  public void getMissingCollection() throws Throwable {

    response().on(server)
      .withStatus(HttpStatus.NOT_FOUND)
      .enqueue();

    restbase.collection("missing")
      .load()
      .block();

    assertEquals(1, server.getRequestCount());
    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "missing"), request.getRequestUrl());
    assertEquals("GET", request.getMethod());
  }

}

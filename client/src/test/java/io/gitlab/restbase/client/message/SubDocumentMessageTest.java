package io.gitlab.restbase.client.message;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.message.TopicEncoder.changesTopic;
import static io.gitlab.restbase.message.TopicEncoder.childrenTopic;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.DocumentOperations;
import io.gitlab.restbase.client.MockRestClientBuilder;
import io.gitlab.restbase.client.RestClient;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.message.ChildEventType;
import io.gitlab.restbase.message.DocumentChildEvent;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SimpleBrokerTestApplication.class)
public class SubDocumentMessageTest {

  static final String NAME = "name";
  static String API_KEY = "testApiKey";

  private Restbase restbase;

  @Value("${local.server.port}")
  private int port;

  private StompSession stompSession;
  private RestClient mockRestClient;

  private Document subDocument;
  private DocumentWithId savedDocument;

  @Before
  public void setup() {
    String rootUrl = "http://localhost:" + port;

    MockRestClientBuilder clientBuilder = new MockRestClientBuilder(config(rootUrl, API_KEY));
    mockRestClient = clientBuilder.getRestClient();
    restbase = new Restbase(clientBuilder);

    stompSession = restbase.getStompSession();

    subDocument = document().with("string1", "test1")
      .with("number1", 123)
      .build();
    savedDocument = new DocumentWithId(42L, path("collection1"), subDocument.getFields());

    when(mockRestClient.post(path("collection1"), subDocument))
      .thenReturn(new DocumentOperations(mockRestClient, clientBuilder.getStompClient(), Mono.just(savedDocument)));
  }

  @Test(timeout = 5000)
  public void valueChangedAndDisconnected() {
    DocumentWithId document = restbase.collection("collection1")
      .add(subDocument)
      .block();

    StepVerifier.create(restbase.changes(document.path()))
      .then(() -> {
        stompSession.send(changesTopic(savedDocument), savedDocument);
      })
      .expectNext(savedDocument)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }

  @Test(timeout = 5000)
  public void addChildAndDisconnect() {
    DocumentWithId document = restbase.collection("collection1")
      .add(subDocument)
      .block();
    DocumentChildEvent added = new DocumentChildEvent(savedDocument.path(), "collection2", ChildEventType.ADDED);

    StepVerifier.create(restbase.children(document.path()))
      .then(() -> {
        stompSession.send(childrenTopic(savedDocument), added);
      })
      .expectNext(added)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }

  @Test(timeout = 5000)
  public void removeChildAndDisconnect() {
    DocumentWithId document = restbase.collection("collection1")
      .add(subDocument)
      .block();
    DocumentChildEvent removed = new DocumentChildEvent(savedDocument.path(), "collection2", ChildEventType.REMOVED);

    StepVerifier.create(restbase.children(document.path()))
      .then(() -> {
        stompSession.send(childrenTopic(savedDocument), removed);
      })
      .expectNext(removed)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }
}

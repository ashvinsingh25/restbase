package io.gitlab.restbase.client;

import static org.mockito.Mockito.mock;

public class MockRestClientBuilder extends ClientBuilder {

  private RestClient mockRestClient;

  public MockRestClientBuilder(RestBaseConfiguration config) {
    super(config);
    mockRestClient = mock(RestClient.class);
  }

  @Override
  public RestClient getRestClient() {
    return mockRestClient;
  }

}

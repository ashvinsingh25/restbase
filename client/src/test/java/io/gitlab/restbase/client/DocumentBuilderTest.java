package io.gitlab.restbase.client;

import static io.gitlab.restbase.client.Restbase.document;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import io.gitlab.restbase.Document;

public class DocumentBuilderTest {

  @Test
  public void build() {
    Document doc = document().with("string1", "test")
      .build();
    assertNotNull(doc);
  }

  @Test
  public void emptyDocument() {
    Document doc = document().build();
    assertNotNull(doc);
  }

  @Test
  public void missingField() {
    Document doc = document().build();
    assertNotNull(doc);
    assertEquals(empty(), doc.get("field1"));
  }

  @Test
  public void stringField() {
    Document doc = document().with("string1", "test")
      .build();
    assertNotNull(doc);
    assertEquals(of("test"), doc.get("string1"));
  }

  @Test
  public void integerField() {
    Document doc = document().with("integer1", 123)
      .build();
    assertNotNull(doc);
    assertEquals(of(123), doc.get("integer1"));
  }

  @Test
  public void doubleField() {
    Document doc = document().with("double1", 123.123d)
      .build();
    assertNotNull(doc);
    assertEquals(of(123.123d), doc.get("double1"));
  }

  @Test
  public void booleanField() {
    Document doc = document().with("boolean1", true)
      .with("boolean2", false)
      .build();
    assertNotNull(doc);
    assertEquals(of(true), doc.get("boolean1"));
    assertEquals(of(false), doc.get("boolean2"));
  }

  @Test
  public void stringArrayField() {
    Document doc = document().with("array", new String[] { "a", "b" })
      .build();
    assertNotNull(doc);
    assertNotNull(doc.get("array"));
    assertArrayEquals(new String[] { "a", "b" }, (String[]) doc.get("array")
      .get());
  }

  @Test
  public void integerArrayField() {
    Document doc = document().with("array", new int[] { 1, 2 })
      .build();
    assertNotNull(doc);
    assertNotNull(doc.get("array"));
    assertArrayEquals(new int[] { 1, 2 }, (int[]) doc.get("array")
      .get());
  }

  @Test
  public void doubleArrayField() {
    Document doc = document().with("array", new double[] { 1.123, 2.123d })
      .build();
    assertNotNull(doc);
    assertNotNull(doc.get("array"));
    assertArrayEquals(new double[] { 1.123, 2.123d }, (double[]) doc.get("array")
      .get(), 0.1d);
  }

  @Test
  public void booleanArrayField() {
    Document doc = document().with("array", new boolean[] { true, false })
      .build();
    assertNotNull(doc);
    assertNotNull(doc.get("array"));
    assertArrayEquals(new boolean[] { true, false }, (boolean[]) doc.get("array")
      .get());
  }
}

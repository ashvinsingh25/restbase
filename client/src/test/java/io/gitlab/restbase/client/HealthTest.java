package io.gitlab.restbase.client;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.restbase;
import static io.gitlab.restbase.test.utils.MockResponseBuilder.response;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.client.Health;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

public class HealthTest {

  static String API_KEY = "testApiKey";

  private RestBaseConfiguration config;
  private Restbase restbase;
  private ObjectMapper objectMapper;
  private MockWebServer server;
  private String rootUrl;
  private String healthUrl;

  @Before
  public void setup() {
    server = new MockWebServer();
    rootUrl = server.url("/") + "";
    healthUrl = rootUrl + "actuator/health";
    config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    objectMapper = restbase.getObjectMapper();
  }

  @After
  public void shutdown() throws Exception {
    server.shutdown();
  }

  @Test
  public void isOnline() throws Throwable {
    Health health = Health.online();
    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, health)
      .enqueue();
    
    Boolean online = restbase.online().block();
    assertTrue(online);

    assertEquals(1, server.getRequestCount());

    RecordedRequest request = server.takeRequest();
    assertEquals(healthUrl, request.getRequestUrl()
      .toString());
    assertEquals("GET", request.getMethod());
  }

}

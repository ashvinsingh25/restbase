package io.gitlab.restbase.client.message;

import static io.gitlab.restbase.client.RestBaseConfiguration.APP_PREFIX;
import static io.gitlab.restbase.message.TopicEncoder.DESTINATION_PREFIX;
import static io.gitlab.restbase.client.RestBaseConfiguration.STOMP_ENDPOINT;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@SpringBootApplication
@Configuration
@EnableWebSocketMessageBroker
public class SimpleBrokerTestApplication implements WebSocketMessageBrokerConfigurer {

  @Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
    registry.enableSimpleBroker(DESTINATION_PREFIX);
    registry.setApplicationDestinationPrefixes(APP_PREFIX);
  }

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint(STOMP_ENDPOINT)
      .setAllowedOrigins("*")
      .withSockJS();
  }

}

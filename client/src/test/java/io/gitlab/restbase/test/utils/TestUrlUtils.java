package io.gitlab.restbase.test.utils;

import java.net.URI;

import org.springframework.web.util.UriComponentsBuilder;

import okhttp3.HttpUrl;

public class TestUrlUtils {

  protected TestUrlUtils() {
  }

  public static URI uri(String base, String... segments) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(base)
      .pathSegment(segments);
    return builder.build()
      .toUri();
  }

  public static HttpUrl url(String base, String... segments) {
    return HttpUrl.get(uri(base, segments));
  }

}

package io.gitlab.restbase.test.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class MockResponseBuilder {

  private MockWebServer server;
  private HttpStatus httpStatus;
  private MediaType contentType;
  private String body;

  public static MockResponseBuilder response() {
    return new MockResponseBuilder();
  }

  public MockResponseBuilder on(MockWebServer server) {
    this.server = server;
    return this;
  }

  public MockResponse enqueue() {
    MockResponse response = new MockResponse();
    if (httpStatus != null) {
      response.setResponseCode(httpStatus.value());
    }
    if (contentType != null) {
      response.setHeader("Content-Type", contentType.toString());
    }
    if (body != null) {
      response.setBody(body);
    }
    if (server != null) {
      server.enqueue(response);
    }
    return response;
  }

  public MockResponseBuilder withStatus(HttpStatus httpStatus) {
    this.httpStatus = httpStatus;
    return this;
  }

  public MockResponseBuilder contentType(MediaType contentType) {
    this.contentType = contentType;
    return this;
  }

  public <B> MockResponseBuilder body(ObjectMapper objectMapper, B body) {
    try {
      this.body = objectMapper.writeValueAsString(body);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return this;
  }

}

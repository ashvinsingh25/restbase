package io.gitlab.restbase.client;

import java.net.URI;

public class CollectionNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private URI path;

  public CollectionNotFoundException(URI path) {
    this.path = path;
  }

  @Override
  public String toString() {
    return "Collection not found [path=" + path + "]";
  }

}

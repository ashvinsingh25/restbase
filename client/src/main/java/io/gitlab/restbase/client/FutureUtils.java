package io.gitlab.restbase.client;

import java.util.concurrent.CompletableFuture;

import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import reactor.core.publisher.Mono;

public class FutureUtils {

  public static <T> Mono<T> monoFromFuture(ListenableFuture<T> future) {
    return Mono.fromFuture(completableFromListenable(future));
  }

  public static <T> Mono<T> monoFromFuture(CompletableFuture<T> future) {
    return Mono.fromFuture(future);
  }

  public static <T> CompletableFuture<T> completableFromListenable(ListenableFuture<T> listenable) {
    return new CompletableFutureAdapter<T>(listenable);
  }

  private static class CompletableFutureAdapter<T> extends CompletableFuture<T> implements ListenableFutureCallback<T> {

    private ListenableFuture<T> listenable;

    public CompletableFutureAdapter(ListenableFuture<T> listenable) {
      this.listenable = listenable;
      listenable.addCallback(this);
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
      boolean result = listenable.cancel(mayInterruptIfRunning);
      super.cancel(mayInterruptIfRunning);
      return result;
    }

    @Override
    public void onSuccess(T result) {
      this.complete(result);
    }

    @Override
    public void onFailure(Throwable t) {
      this.completeExceptionally(t);
    }

  }
}

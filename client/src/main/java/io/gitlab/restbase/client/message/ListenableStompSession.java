package io.gitlab.restbase.client.message;

import java.util.ArrayList;
import java.util.List;

import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;

public class ListenableStompSession implements StompSession {

  private StompSession session;
  private List<StompSessionListener> listeners;

  public ListenableStompSession(StompSession stompSession) {
    this.session = stompSession;
    this.listeners = new ArrayList<>();
  }

  @Override
  public String getSessionId() {
    return session.getSessionId();
  }

  @Override
  public boolean isConnected() {
    return session.isConnected();
  }

  @Override
  public void setAutoReceipt(boolean enabled) {
    session.setAutoReceipt(enabled);
  }

  @Override
  public Receiptable send(String destination, Object payload) {
    return session.send(destination, payload);
  }

  @Override
  public Receiptable send(StompHeaders headers, Object payload) {
    return session.send(headers, payload);
  }

  @Override
  public Subscription subscribe(String destination, StompFrameHandler handler) {
    return session.subscribe(destination, handler);
  }

  @Override
  public Subscription subscribe(StompHeaders headers, StompFrameHandler handler) {
    return session.subscribe(headers, handler);
  }

  @Override
  public Receiptable acknowledge(String messageId, boolean consumed) {
    return session.acknowledge(messageId, consumed);
  }

  @Override
  public Receiptable acknowledge(StompHeaders headers, boolean consumed) {
    return session.acknowledge(headers, consumed);
  }

  @Override
  public void disconnect() {
    listeners.forEach(l -> l.disconnected(this));
    session.disconnect();
  }

  @Override
  public void disconnect(StompHeaders headers) {
    listeners.forEach(l -> l.disconnected(this));
    session.disconnect(headers);
  }

  public void addSessionListener(StompSessionListener listener) {
    listeners.add(listener);
  }

  public void removeSessionListener(StompSessionListener listener) {
    listeners.remove(listener);
  }

}

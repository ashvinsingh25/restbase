package io.gitlab.restbase.client;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.message.StompClient;
import reactor.core.publisher.Mono;

public class DocumentOperations {

  private final RestClient restClient;
  private final StompClient stompClient;
  private final Mono<DocumentWithId> documentMono;
  private DocumentWithId document;

  public DocumentOperations(RestClient restClient, StompClient stompClient, Mono<DocumentWithId> document) {
    this.restClient = restClient;
    this.stompClient = stompClient;
    this.documentMono = document;
  }

  public DocumentWithId block() {
    if (document == null) {
      document = documentMono.block();
    }
    return document;
  }

  public CollectionOperations collection(String name) {
    Path path = block().path();
    return new CollectionOperations(restClient, stompClient, path, name);
  }

}

package io.gitlab.restbase.client.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.messaging.simp.stomp.StompSession;

public class StompSubscriptions {

  private Map<String, List<SubscriptionSink<?>>> topicToSubscriptionList = new HashMap<>();
  private List<SubscriptionSink<?>> allList = new ArrayList<>();

  public void add(String topic, SubscriptionSink<?> subscription) {
    List<SubscriptionSink<?>> list = topicToSubscriptionList.get(topic);
    if (list == null) {
      list = new ArrayList<>();
      topicToSubscriptionList.put(topic, list);
    }
    list.add(subscription);
    allList.add(subscription);
  }

  public void disconnect(StompSession stompSession) {
    allList.forEach(s-> {
      s.sink.complete();
    });
    allList.clear();
    topicToSubscriptionList.clear();
  }

}

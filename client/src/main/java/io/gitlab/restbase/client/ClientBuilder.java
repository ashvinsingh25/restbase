package io.gitlab.restbase.client;

import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.client.message.StompClient;

class ClientBuilder {

  final RestBaseConfiguration configuration;

  private ObjectMapper objectMapper;
  private StompClient stompClient;
  private RestClient restClient;

  public ClientBuilder(RestBaseConfiguration configuration) {
    this.configuration = configuration;
  }

  public ObjectMapper getObjectMapper() {
    if (objectMapper == null) {
      objectMapper = Jackson2ObjectMapperBuilder.json()
        .build();
      objectMapper.configure(FAIL_ON_EMPTY_BEANS, false);
    }
    return objectMapper;
  }

  public StompClient getStompClient() {
    if (stompClient == null) {
      stompClient = new StompClient(configuration, getObjectMapper());
    }
    return stompClient;
  }

  public RestClient getRestClient() {
    if (restClient == null) {
      restClient = new RestClient(configuration, getObjectMapper(), getStompClient());
    }
    return restClient;
  }

}

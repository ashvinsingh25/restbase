package io.gitlab.restbase.client.message;

import static io.gitlab.restbase.message.TopicEncoder.changesTopic;
import static io.gitlab.restbase.message.TopicEncoder.childrenTopic;

import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSession.Subscription;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.FutureUtils;
import io.gitlab.restbase.message.ChildEvent;
import io.gitlab.restbase.message.CollectionChildEvent;
import io.gitlab.restbase.message.DocumentChildEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/* handles lazy loading of sessions */
public class SessionManager implements StompSessionListener {

  final Logger logger = LoggerFactory.getLogger(getClass());

  private final WebSocketStompClient client;
  private final String url;
  private final StompSubscriptions subscriptions;
  private Mono<StompSession> sessionMono;
  private StompSession session;

  public SessionManager(WebSocketStompClient client, String url) {
    this.client = client;
    this.url = url;
    this.subscriptions = new StompSubscriptions();
  }

  public Mono<StompSession> sessionMono() {
    if (sessionMono == null) {
      ListenableFuture<StompSession> session = client.connect(url, new StompSessionHandlerAdapter() {

        @Override
        public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload,
            Throwable exception) {
          logger.warn("stomp exception", exception);
        }

        @Override
        public void handleTransportError(StompSession session, Throwable exception) {
          logger.warn("stomp transport error", exception);
        }

      });
      this.sessionMono = FutureUtils.monoFromFuture(session);
    }
    return this.sessionMono;
  }

  public StompSession session() {
    if (session == null) {
      ListenableStompSession listenable = new ListenableStompSession(sessionMono().block());
      listenable.addSessionListener(this);
      this.session = listenable;
    }
    return this.session;
  }

  @Override
  public void disconnected(StompSession stompSession) {
    subscriptions.disconnect(stompSession);
  }

  public Flux<DocumentWithId> subscribe(Path document) {
    String topic = changesTopic(document);
    return subscribe(topic);
  }

  private Flux<DocumentWithId> subscribe(String topic) {
    StompSession session = session();
    return Flux.create(sink -> {
      logger.info("subscribe: {}", topic);
      Subscription subscription = session.subscribe(topic, new StompFrameHandler() {

        @Override
        public Type getPayloadType(StompHeaders headers) {
          return DocumentWithId.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
          sink.next((DocumentWithId) payload);
        }
      });

      subscriptions.add(topic, new SubscriptionSink<>(subscription, sink));
    });
  }

  public Flux<DocumentChildEvent> subscribeChildren(DocumentWithId document) {
    String topic = childrenTopic(document);
    logger.info("subscribe children: {}", topic);
    return documentChildren(topic);
  }

  public Flux<DocumentChildEvent> subscribeChildren(Mono<DocumentWithId> document) {
    return document.flatMapMany(this::subscribeChildren);
  }

  private Flux<DocumentChildEvent> documentChildren(String topic) {
    StompSession session = session();
    return Flux.create(sink -> {
      logger.info("subscribe: {}", topic);
      Subscription subscription = session.subscribe(topic, new StompFrameHandler() {

        @Override
        public Type getPayloadType(StompHeaders headers) {
          return DocumentChildEvent.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
          sink.next((DocumentChildEvent) payload);
        }
      });

      subscriptions.add(topic, new SubscriptionSink<>(subscription, sink));
    });
  }

  public Flux<CollectionChildEvent> subscribeChildren(Collection collection) {
    String topic = childrenTopic(collection);
    return collectionChildren(topic);
  }

  private Flux<CollectionChildEvent> collectionChildren(String topic) {
    StompSession session = session();
    return Flux.create(sink -> {
      logger.info("subscribe: {}", topic);
      Subscription subscription = session.subscribe(topic, new StompFrameHandler() {

        @Override
        public Type getPayloadType(StompHeaders headers) {
          return CollectionChildEvent.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
          sink.next((CollectionChildEvent) payload);
        }
      });

      subscriptions.add(topic, new SubscriptionSink<>(subscription, sink));
    });
  }

  public Flux<ChildEvent> subscribeChildren(Path path) {
    String topic = childrenTopic(path);
    return path.isCollection() ? collectionChildren(topic).map(e -> e) : documentChildren(topic).map(e -> e);
  }

}

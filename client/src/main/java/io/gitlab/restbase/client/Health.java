package io.gitlab.restbase.client;

public class Health {

  private Status status;

  public Status getStatus() {
    return status;
  }

  public Boolean isHealhy() {
    return Status.UP == status;
  }

  public static enum Status {
    UNKNOWN, UP, DOWN, OUT_OF_SERVICE,
  }

  public static Health online() {
    Health health = new Health();
    health.status = Status.UP;
    return health;
  }
}

package io.gitlab.restbase.client;

import static io.gitlab.restbase.PathUtils.path;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.codec.ClientCodecConfigurer.ClientDefaultCodecs;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.message.StompClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class RestClient {

  private static final String URL_EXT_COLLECTIONS = "collections";

  private static final Class<DocumentWithId> DOCUMENT_TYPE = DocumentWithId.class;

  private static final Class<Collection> COLLECTION_TYPE = Collection.class;

  final Log logger = LogFactory.getLog(getClass());

  private final URI healthcheckUrl;

  private final WebClient webClient;

  private final RestBaseConfiguration config;

  private final StompClient stompClient;

  public RestClient(RestBaseConfiguration config, ObjectMapper objectMapper, StompClient stompClient) {
    this.config = config;
    this.stompClient = stompClient;
    ExchangeStrategies strategies = ExchangeStrategies.builder()
      .codecs(clientDefaultCodecsConfigurer -> {
        ClientDefaultCodecs defaultCodecs = clientDefaultCodecsConfigurer.defaultCodecs();
        defaultCodecs.jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper, APPLICATION_JSON));
        defaultCodecs.jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, APPLICATION_JSON));
      })
      .build();

    this.healthcheckUrl = actuator("health");
    String root = config.getUrl();
    this.webClient = WebClient.builder()
      .exchangeStrategies(strategies)
      .baseUrl(root)
      .build();
  }

  private URI api(Path path) {
    return uri("api", path);
  }

  private URI api(Path path, Long id) {
    return uri("api", path, id);
  }

  private URI api(Path path, Long id, String ext) {
    return uri("api", path, id, ext);
  }

  private URI actuator(String path) {
    return uri("actuator", path(path));
  }

  private URI uri(String prefix, Path path, Long id, String... segments) {
    if (Document.ROOT_ID == id || id == null) {
      return uri(prefix, path, segments);
    }
    String[] newSegments = new String[segments.length + 1];
    newSegments[0] = "" + id;
    System.arraycopy(segments, 0, newSegments, 1, segments.length);
    return uri(prefix, path, newSegments);
  }

  private URI uri(String prefix, Path path, String... segments) {
    String root = config.getUrl();
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(root)
      .pathSegment(prefix);
    if (path != null) {
      String[] pathSegments = path.getSegments()
        .stream()
        .toArray(String[]::new);
      builder = builder.pathSegment(pathSegments);
    }
    builder = builder.pathSegment(segments);
    return builder.build()
      .toUri();
  }

  public DocumentOperations post(Path path, Document document) {
    URI uri = api(path);
    return new DocumentOperations(this, stompClient, webClient.post()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .body(fromValue(document))
      .retrieve()
      .bodyToMono(DOCUMENT_TYPE)
      .switchIfEmpty(Mono.error(() -> new DocumentNotFoundException(uri)))
      .onErrorMap((err) -> new DocumentNotFoundException(uri)));
  }

  public DocumentOperations put(DocumentWithId document) {
    URI uri = api(document.path());
    return new DocumentOperations(this, stompClient, webClient.put()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .body(fromValue(document))
      .retrieve()
      .bodyToMono(DOCUMENT_TYPE)
      .switchIfEmpty(Mono.error(() -> new DocumentNotFoundException(uri)))
      .onErrorMap((err) -> new DocumentNotFoundException(uri)));
  }

  public Mono<DocumentWithId> delete(DocumentWithId documentWithId) {
    Path collection = documentWithId.getParent();
    URI uri = api(collection, documentWithId.getId());
    return webClient.delete()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToMono(DOCUMENT_TYPE)
      .switchIfEmpty(Mono.error(() -> new DocumentNotFoundException(uri)))
      .onErrorMap((err) -> new DocumentNotFoundException(uri));
  }

  public Mono<DocumentWithId> delete(Path path, Long id) {
    URI uri = api(path, id);
    return webClient.delete()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToMono(DOCUMENT_TYPE)
      .switchIfEmpty(Mono.error(() -> new DocumentNotFoundException(uri)))
      .onErrorMap((err) -> new DocumentNotFoundException(uri));
  }

  public Mono<DocumentWithId> get(Path path, Long id) {
    URI uri = api(path, id);
    return webClient.get()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToMono(DOCUMENT_TYPE)
      .switchIfEmpty(Mono.error(() -> new DocumentNotFoundException(uri)))
      .onErrorMap((err) -> new DocumentNotFoundException(uri));
  }

  public Flux<DocumentWithId> list(Path path) {
    URI url = api(path);
    return webClient.get()
      .uri(url)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToFlux(DOCUMENT_TYPE);
  }

  public Mono<Collection> post(Path path, Collection collection) {
    URI uri = api(path);
    return webClient.post()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .body(fromValue(collection))
      .retrieve()
      .bodyToMono(COLLECTION_TYPE);
  }

  public Mono<Collection> delete(Path path) {
    URI uri = api(path);
    return webClient.delete()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToMono(COLLECTION_TYPE)
      .switchIfEmpty(Mono.error(() -> new CollectionNotFoundException(uri)))
      .onErrorMap((err) -> new CollectionNotFoundException(uri));
  }

  public Mono<Collection> get(Path path) {
    URI uri = api(path);
    return webClient.get()
      .uri(uri)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToMono(COLLECTION_TYPE)
      .switchIfEmpty(Mono.error(() -> new CollectionNotFoundException(uri)))
      .onErrorMap((err) -> new CollectionNotFoundException(uri));
  }

  public Flux<Collection> list(DocumentWithId document) {
    Path collection = document.getParent();
    URI url = api(collection, document.getId(), URL_EXT_COLLECTIONS);
    return webClient.get()
      .uri(url)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToFlux(COLLECTION_TYPE);
  }

  public Mono<Boolean> healthcheck() {
    return webClient.get()
      .uri(healthcheckUrl)
      .accept(APPLICATION_JSON)
      .retrieve()
      .bodyToMono(Health.class)
      .map(Health::isHealhy);
  }

}

package io.gitlab.restbase.client;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.message.StompClient;
import io.gitlab.restbase.message.CollectionChildEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class CollectionOperations {

  private final RestClient restClient;
  private final StompClient stompClient;
  private final Collection collection;

  public CollectionOperations(RestClient restClient, StompClient stompClient, Path path, String name) {
    this.restClient = restClient;
    this.stompClient = stompClient;
    this.collection = new Collection(path, name);
  }

  public DocumentOperations add(DocumentBuilder builder) {
    return add(builder.build());
  }

  public DocumentOperations add(Document d) {
    return restClient.post(collection.path(), d);
  }

  public Mono<DocumentWithId> document(Long id) {
    return restClient.get(collection.path(), id);
  }

  public Mono<Collection> add() {
    return restClient.post(collection.getParent(), collection);
  }

  public Mono<Collection> load() {
    return restClient.get(collection.path());
  }

  public Mono<Collection> delete() {
    return restClient.delete(collection.path());
  }

  public Flux<CollectionChildEvent> children() {
    return stompClient.subscribeChildren(collection);
  }


}

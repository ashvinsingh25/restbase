package io.gitlab.restbase.client.message;

import java.util.List;

import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.message.ChildEvent;
import io.gitlab.restbase.message.CollectionChildEvent;
import io.gitlab.restbase.message.DocumentChildEvent;
import reactor.core.publisher.Flux;

public class StompClient {

  public static final String TOPIC_CHANGES = "changes";

  private final WebSocketStompClient client;
  private final SessionManager sessionManager;

  public StompClient(RestBaseConfiguration config, ObjectMapper objectMapper) {
    this.client = new WebSocketStompClient(new SockJsClient(webSocketTransport()));

    MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
    messageConverter.setObjectMapper(objectMapper);
    this.client.setMessageConverter(messageConverter);

    String url = config.getSocketUrl();
    this.sessionManager = new SessionManager(client, url);
  }

  private List<Transport> webSocketTransport() {
    return Lists.newArrayList(new WebSocketTransport(new StandardWebSocketClient()));
  }

  public StompSession session() {
    return this.sessionManager.session();
  }

  public Flux<DocumentWithId> subscribeChanges(Path documentPath) {
    return sessionManager.subscribe(documentPath);
  }

  public Flux<DocumentChildEvent> subscribeChildren(DocumentWithId document) {
    return sessionManager.subscribeChildren(document);
  }

  public Flux<CollectionChildEvent> subscribeChildren(Collection collection) {
    return sessionManager.subscribeChildren(collection);
  }

  public Flux<ChildEvent> subscribeChildren(Path path) {
    return sessionManager.subscribeChildren(path);
  }

}

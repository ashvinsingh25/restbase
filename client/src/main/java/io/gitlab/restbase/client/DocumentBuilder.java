package io.gitlab.restbase.client;

import java.util.HashMap;
import java.util.Map;

import io.gitlab.restbase.Document;

public class DocumentBuilder {

  private Map<String, Object> fields;

  public DocumentBuilder(Map<String, Object> fields) {
    super();
    this.fields = fields;
  }

  public DocumentBuilder() {
    this.fields = new HashMap<>();
  }

  public Document build() {
    return new Document(fields);
  }

  public DocumentBuilder with(String field, String value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, int value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, double value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, boolean value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, String[] value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, int[] value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, double[] value) {
    fields.put(field, value);
    return this;
  }

  public DocumentBuilder with(String field, boolean[] value) {
    fields.put(field, value);
    return this;
  }

}

package io.gitlab.restbase.client;

import java.net.URI;

public class DocumentNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private URI path;

  public DocumentNotFoundException(URI path) {
    this.path = path;
  }

  @Override
  public String toString() {
    return "Document not found [path=" + path + "]";
  }

}

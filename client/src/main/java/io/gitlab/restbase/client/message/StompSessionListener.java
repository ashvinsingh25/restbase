package io.gitlab.restbase.client.message;

import org.springframework.messaging.simp.stomp.StompSession;

public interface StompSessionListener {

  void disconnected(StompSession stompSession);

}
